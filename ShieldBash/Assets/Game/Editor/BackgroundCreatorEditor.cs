﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BackgroundCreator))]
public class BackgroundCreatorEditor : Editor
{
    BackgroundCreator creator;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        creator = (BackgroundCreator)target;

        GUIStyle boldText = new GUIStyle(GUI.skin.label);
        boldText.fontStyle = FontStyle.Bold;

        GUILayout.Space(10);
        GUILayout.Label("Background Size", boldText);
        GUILayout.BeginHorizontal();
        creator.rows = EditorGUILayout.IntField("Rows", creator.rows, GUILayout.ExpandWidth(true));
        creator.columns = EditorGUILayout.IntField("Columns", creator.columns, GUILayout.ExpandWidth(true));
        GUILayout.EndHorizontal();

        GUILayout.Space(10);
        GUILayout.Label("Spacing",boldText);
        GUILayout.BeginHorizontal();
        creator.xSpacing = EditorGUILayout.FloatField("X", creator.xSpacing, GUILayout.ExpandWidth(true));
        creator.ySpacing = EditorGUILayout.FloatField("Y", creator.ySpacing, GUILayout.ExpandWidth(true));
        GUILayout.EndHorizontal();

        GUILayout.Space(10);
        if (GUILayout.Button("Create Background", GUILayout.ExpandWidth(true)))
        {
            creator.CreateBackground();
        }
        GUILayout.Space(5);
        if (GUILayout.Button("Clear Background", GUILayout.ExpandWidth(true)))
        {
            creator.ClearBackground();
        }
    }
}