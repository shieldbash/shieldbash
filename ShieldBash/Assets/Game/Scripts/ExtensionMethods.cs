﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static T InstantiateInside<T>(this Transform obj, GameObject prefab)
    {
        GameObject _obj = Transform.Instantiate(prefab, obj);

        return _obj.GetComponent<T>();
    }
}