﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShieldStates { Closed = 0, Reflecting = 1, Blocking = 2, Count = 3 }

public class ShieldStateMachine : MonoBehaviour
{
    [Header("Character Behaviour Manager")]
    public CharacterBehaviour behaviour;

    [Header("Shield Properties")]
    public float reflectingTime;
    public Transform shieldTransform;
    public SpriteRenderer shieldSprite;

    ShieldStates currentState = ShieldStates.Closed;

    public ShieldState[] states { get; private set; }

    private void Awake()
    {
        states = new ShieldState[(int)ShieldStates.Count];

        states[(int)ShieldStates.Closed] = new ShieldClosedState();
        states[(int)ShieldStates.Reflecting] = new ShieldReflectingState() { reflectingTime = reflectingTime };
        states[(int)ShieldStates.Blocking] = new ShieldBlockingState();

        for (int i = 0; i < (int)ShieldStates.Count; i++)
        {
            states[i].stateMachine = this;
        }
    }

    void Update()
    {
        this.UpdateCurrentState();
    }

    private void FixedUpdate()
    {
        this.FixedUpdateCurrentState();
    }

    void UpdateCurrentState()
    {
        states[(int)currentState].UpdateState();

        if (states[(int)currentState].ShouldExitState())
        {
            this.ChangeState();
        }
    }

    void FixedUpdateCurrentState()
    {
        states[(int)currentState].FixedUpdateState();
    }

    void ChangeState()
    {
        ShieldStates lastState = currentState;

        for (int i = 0; i < (int)StateMachineStates.Count; i++)
        {
            if (i == (int)currentState) continue;

            if (states[i].ShouldEnterState())
            {
                states[(int)currentState].OnExitState();

                currentState = (ShieldStates)i;

                states[(int)currentState].OnEnterState();

                Debug.Log("Changed from shield state " + lastState + " to shield state " + currentState);

                return;
            }
        }
        currentState = ShieldStates.Closed;
    }
}