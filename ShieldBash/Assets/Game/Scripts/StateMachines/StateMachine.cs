﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateMachineStates { None = -1, Stand = 0, Walk = 1, Run = 2, Roll = 3, Jump = 4, Fall = 5, Hit = 6, Count = 7 }

public class StateMachine : MonoBehaviour
{
    new public Rigidbody2D rigidbody2D { get; private set; }

    public int direction { get; set; }

    [Header("Character Behaviour Manager")]
    public CharacterBehaviour behaviour;

    [Header("Character Velocities")]
    public float walkSpeed;
    public float timeToWalkSpeed;
    public float runSpeed;
    public float timeToRunSpeed;
    public float rollOffset;
    public float rollTime;
    public float jumpSpeed;

    StateMachineStates currentState = StateMachineStates.None;

    State[] states;

    void Awake()
    {
        this.rigidbody2D = this.GetComponent<Rigidbody2D>();
        direction = 1;

        states = new State[(int)StateMachineStates.Count];

        states[(int)StateMachineStates.Stand] = new StandState();
        states[(int)StateMachineStates.Walk] = new WalkState();
        states[(int)StateMachineStates.Run] = new RunState();
        states[(int)StateMachineStates.Roll] = new RollState();
        states[(int)StateMachineStates.Jump] = new JumpState();
        states[(int)StateMachineStates.Fall] = new FallState();
        states[(int)StateMachineStates.Hit] = new HitState();

        for (int i = 0; i < (int)StateMachineStates.Count; i++)
        {
            states[i].stateMachine = this;
        }

        currentState = StateMachineStates.Stand;

    }

    void Update()
    {
        if (currentState != StateMachineStates.None)
            this.UpdateCurrentState();
    }

    private void FixedUpdate()
    {
        if (currentState != StateMachineStates.None)
            this.FixedUpdateCurrentState();
    }

    void UpdateCurrentState()
    {
        states[(int)currentState].UpdateState();

        if(states[(int)currentState].ShouldExitState())
        {
            this.CheckStateChange();
        }
    }

    void FixedUpdateCurrentState()
    {
        states[(int)currentState].FixedUpdateState();
    }

    void CheckStateChange()
    {
        StateMachineStates lastState = currentState;

        for (int i = 0; i < (int)StateMachineStates.Count; i++)
        {
            if (i == (int)currentState) continue;

            if(states[i].ShouldEnterState())
            {
                states[(int)currentState].OnExitState();

                currentState = (StateMachineStates)i;

                states[(int)currentState].OnEnterState();

                Debug.Log("Changed from state " + lastState + " to state " + currentState);

                return;
            }
        }

        Debug.LogWarning("exited state "+currentState+", entered None. This should not happen. Setting state as Stand, to failproof stuff");
        currentState = StateMachineStates.Stand;
    }

    public void ChangeState(StateMachineStates newState)
    {
        states[(int)currentState].OnExitState();

        currentState = newState;

        states[(int)currentState].OnEnterState();
    }
}