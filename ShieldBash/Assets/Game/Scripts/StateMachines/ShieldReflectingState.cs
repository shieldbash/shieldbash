﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldReflectingState : ShieldState
{
    public float reflectingTime { get; set; }
    public float reflectingCounter { get; private set; }

    public override bool ShouldEnterState()
    {
        return stateMachine.behaviour.GetShieldOpen();
    }

    public override bool ShouldExitState()
    {
        return !stateMachine.behaviour.GetShieldOpen() || this.IsReflectingOver();
    }

    public override void UpdateState()
    {
        reflectingCounter += Time.deltaTime;

        Vector2 shieldPosition = stateMachine.behaviour.GetShieldAxis();

        if (CloseEnoughToZero(shieldPosition)) return;

        shieldPosition.Normalize();

        stateMachine.shieldTransform.up = shieldPosition;
        stateMachine.shieldTransform.position = this.SumVector3AndVector2(stateMachine.transform.position, shieldPosition);
    }

    public override void OnEnterState()
    {
        stateMachine.shieldSprite.color = Color.green;
    }

    public void ResetCounter()
    {
        reflectingCounter = 0;
    }

    public bool IsReflectingOver()
    {
        return reflectingCounter >= reflectingTime;
    }

    Vector3 SumVector3AndVector2(Vector3 v3, Vector2 v2)
    {
        v3.x += v2.x;
        v3.y += v2.y;

        return v3;
    }

    bool CloseEnoughToZero(Vector2 v)
    {
        return ((v.x > -0.05f && v.x < 0.05f) && (v.y > -0.05f && v.y < 0.05f));
    }
}