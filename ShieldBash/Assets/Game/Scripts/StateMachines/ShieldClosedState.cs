﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldClosedState : ShieldState
{
    public override bool ShouldEnterState()
    {
        return !stateMachine.behaviour.GetShieldOpen();
    }

    public override bool ShouldExitState()
    {
        return stateMachine.behaviour.GetShieldOpen();
    }

    public override void OnEnterState()
    {
        stateMachine.shieldTransform.gameObject.SetActive(false);
        (stateMachine.states[(int)ShieldStates.Reflecting] as ShieldReflectingState).ResetCounter();
    }

    public override void OnExitState()
    {
        stateMachine.shieldTransform.gameObject.SetActive(true);
    }
}