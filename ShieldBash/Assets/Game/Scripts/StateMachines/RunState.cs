﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunState : State
{
    float targetVelocity;

    public override bool ShouldEnterState()
    {
        if (stateMachine.behaviour.GetMovementAxis() != Vector2.zero &&
            stateMachine.behaviour.GetCharacterAction() == CharacterActions.Run &&
            (stateMachine.rigidbody2D.velocity.y < 0.05f && stateMachine.rigidbody2D.velocity.y > -0.05f))
            return true;

        return false;
    }

    public override bool ShouldExitState()
    {
        //if (stateMachine.behaviour.GetMovementAxis() == Vector2.zero)
        if (stateMachine.rigidbody2D.velocity.x < 0.05f && stateMachine.rigidbody2D.velocity.x > -0.05f)
            return true;

        if (stateMachine.behaviour.GetCharacterAction() != CharacterActions.Run)
            return true;

        if (stateMachine.rigidbody2D.velocity.y > 0.05f || stateMachine.rigidbody2D.velocity.y < -0.05f)
            return true;

        return false;
    }

    public override void FixedUpdateState()
    {
        targetVelocity = stateMachine.runSpeed * stateMachine.behaviour.GetMovementAxis().x;

        Vector2 velocity = stateMachine.rigidbody2D.velocity;

        if (stateMachine.timeToRunSpeed != 0)
            velocity.x = Mathf.Lerp(velocity.x, targetVelocity, Time.fixedDeltaTime / stateMachine.timeToRunSpeed);
        else
            velocity.x = targetVelocity;

        stateMachine.rigidbody2D.velocity = velocity;

        stateMachine.direction = (int)Mathf.Sign(velocity.x);
    }
}