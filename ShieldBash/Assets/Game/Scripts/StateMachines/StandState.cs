﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandState : State
{
    public override bool ShouldEnterState()
    {
        if (stateMachine.behaviour.GetMovementAxis() == Vector2.zero &&
            stateMachine.behaviour.GetCharacterAction() == CharacterActions.None &&
            (stateMachine.rigidbody2D.velocity.y < 0.05f && stateMachine.rigidbody2D.velocity.y > -0.05f))
            return true;

        return false;
    }

    public override bool ShouldExitState()
    {
        if(stateMachine.behaviour.GetMovementAxis() != Vector2.zero)
            return true;

        if (stateMachine.behaviour.GetCharacterAction() != CharacterActions.None)
            return true;

        if (stateMachine.rigidbody2D.velocity.y > 0.05f || stateMachine.rigidbody2D.velocity.y < -0.05f)
            return true;

        return false;
    }
}