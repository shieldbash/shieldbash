﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollState : State
{
    Vector3 rollStartPosition;
    float rollTime;

    public override bool ShouldEnterState()
    {
        if (stateMachine.behaviour.GetCharacterAction() == CharacterActions.DodgeRoll &&
            (stateMachine.rigidbody2D.velocity.y < 0.05f && stateMachine.rigidbody2D.velocity.y > -0.05f))
            return true;

        return false;
    }

    public override bool ShouldExitState()
    {
        if (rollTime > stateMachine.rollTime)
            return true;

        return false;
    }

    public override void UpdateState()
    {
        rollTime += Time.deltaTime;

        float offset = Mathf.Lerp(0, stateMachine.rollOffset, rollTime * (1f / stateMachine.rollTime));
        
        stateMachine.transform.position = rollStartPosition + Vector3.right * offset * stateMachine.direction;
    }

    public override void OnEnterState()
    {
        rollStartPosition = stateMachine.transform.position;
        rollTime = 0;
    }
}