﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallState : State
{
    float targetVelocity;

    public override bool ShouldEnterState()
    {
        if (stateMachine.rigidbody2D.velocity.y < 0.05f &&
            stateMachine.behaviour.GetCharacterAction() == CharacterActions.None)
            return true;

        return false;
    }

    public override bool ShouldExitState()
    {
        if (stateMachine.rigidbody2D.velocity.y < 0.05f && stateMachine.rigidbody2D.velocity.y > -0.05f)
            return true;

        return false;
    }

    public override void FixedUpdateState()
    {
        targetVelocity = stateMachine.walkSpeed * stateMachine.behaviour.GetMovementAxis().x;

        Vector2 velocity = stateMachine.rigidbody2D.velocity;

        if (stateMachine.timeToWalkSpeed != 0)
            velocity.x = Mathf.Lerp(velocity.x, targetVelocity, Time.fixedDeltaTime / stateMachine.timeToWalkSpeed);
        else
            velocity.x = targetVelocity;

        stateMachine.rigidbody2D.velocity = velocity;

        stateMachine.direction = (int)Mathf.Sign(velocity.x);
    }
}