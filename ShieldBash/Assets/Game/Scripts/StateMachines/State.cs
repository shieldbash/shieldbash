﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    public StateMachine stateMachine;

    public virtual void Initialize()
    {

    }

    public virtual bool ShouldEnterState()
    {
        return false;
    }

    public virtual bool ShouldExitState()
    {
        return false;
    }

    public virtual void OnEnterState()
    {

    }

    public virtual void OnExitState()
    {

    }

    public virtual void UpdateState()
    {

    }

    public virtual void FixedUpdateState()
    {

    }
}