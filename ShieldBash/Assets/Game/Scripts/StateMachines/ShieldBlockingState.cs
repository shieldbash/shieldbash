﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBlockingState : ShieldState
{
    public override bool ShouldEnterState()
    {
        return stateMachine.behaviour.GetShieldOpen() && (stateMachine.states[(int)ShieldStates.Reflecting] as ShieldReflectingState).IsReflectingOver();
    }

    public override bool ShouldExitState()
    {
        return !stateMachine.behaviour.GetShieldOpen();
    }

    public override void UpdateState()
    {
        Vector2 shieldPosition = stateMachine.behaviour.GetShieldAxis();

        if (CloseEnoughToZero(shieldPosition)) return;

        shieldPosition.Normalize();

        stateMachine.shieldTransform.up = shieldPosition;
        stateMachine.shieldTransform.position = this.SumVector3AndVector2(stateMachine.transform.position, shieldPosition);
    }

    public override void OnEnterState()
    {
        stateMachine.shieldSprite.color = Color.red;
    }

    Vector3 SumVector3AndVector2(Vector3 v3, Vector2 v2)
    {
        v3.x += v2.x;
        v3.y += v2.y;

        return v3;
    }

    bool CloseEnoughToZero(Vector2 v)
    {
        return ((v.x > -0.05f && v.x < 0.05f) && (v.y > -0.05f && v.y < 0.05f));
    }
}