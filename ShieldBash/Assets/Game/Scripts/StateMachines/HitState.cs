﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitState : State
{
    float stateTime = 1;
    float positionOffset;

    float currentTime;

    public override bool ShouldEnterState()
    {
        return false;
    }

    public override bool ShouldExitState()
    {
        return currentTime >= stateTime;
    }

    public override void OnEnterState()
    {
        currentTime = 0;
    }

    public override void FixedUpdateState()
    {
        
    }

    public override void UpdateState()
    {
        currentTime += Time.deltaTime;
    }
}