﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundCreator : MonoBehaviour
{
    public GameObject[] backgroundPrefabs;

    [HideInInspector]
    public int rows;
    [HideInInspector]
    public int columns;

    [HideInInspector]
    public float xSpacing;
    [HideInInspector]
    public float ySpacing;

    public void CreateBackground()
    {
        this.ClearBackground();

        Vector3 objPosition = new Vector3();
        float yOffset = 0;

        BackgroundItem lastItem = null;

        for (int i = 0; i < rows; i++)
        {
            objPosition.x = 0;
            for (int j = 0; j < columns; j++)
            {
                BackgroundItem newItem;
                if(lastItem == null)
                {
                    newItem = this.transform.InstantiateInside<BackgroundItem>(backgroundPrefabs[Random.Range(0, backgroundPrefabs.Length)]);
                }
                else
                {
                    newItem = this.transform.InstantiateInside<BackgroundItem>(lastItem.possibleNeighbours[Random.Range(0, lastItem.possibleNeighbours.Length)].gameObject);
                }
                newItem.boxCollider2D.enabled = true;

                newItem.transform.localPosition = objPosition;

                objPosition.x += newItem.boxCollider2D.bounds.size.x + xSpacing;

                if (yOffset < newItem.boxCollider2D.bounds.size.y) yOffset = newItem.boxCollider2D.bounds.size.y;

                lastItem = newItem;

                newItem.boxCollider2D.enabled = false;
            }
            objPosition.y -= yOffset - ySpacing;
        }
    }

    public void ClearBackground()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }
}