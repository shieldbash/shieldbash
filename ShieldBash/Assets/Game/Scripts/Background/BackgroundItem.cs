﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundItem : MonoBehaviour
{
    public BackgroundItem[] possibleNeighbours;
    public BoxCollider2D boxCollider2D;
}