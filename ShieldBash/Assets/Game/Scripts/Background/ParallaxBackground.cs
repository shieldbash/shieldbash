﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField]
    CameraManager cameraManager;

    public float parallaxVelocity;
    
    void Update()
    {
        this.transform.position += Vector3.left * parallaxVelocity  * cameraManager.amountMoved;
    }
}