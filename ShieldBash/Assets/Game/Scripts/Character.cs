﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public static Character Instance { get; private set; }

    [SerializeField]
    int startingLife;

    public int currentLife { get; private set; }

    StateMachine stateMachine;

    private void Awake()
    {
        Instance = this;

        stateMachine = this.GetComponent<StateMachine>();
        currentLife = startingLife;
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        switch (target.tag)
        {
            case "CollidableEnemy":
            case "Projectile":
                this.OnDamageableHit(target.GetComponent<Damageable>());
                break;
        }
    }

    void OnDamageableHit(Damageable damageable)
    {
        if(damageable == null)
        {
            Debug.LogError("Collided with someone that should have the Damageable component, but doesn't.");
            return;
        }

        this.currentLife -= damageable.damageAmount;

        if (this.currentLife >= 0)
        {
            stateMachine.ChangeState(StateMachineStates.Hit);
        }
        else
        {
            //TODO: Kill this motherfucker
        }
    }
}