﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterActions { None, Jump, DodgeRoll, Run }

public class CharacterBehaviour : MonoBehaviour
{
    public virtual Vector2 GetMovementAxis()
    {
        return Vector2.zero;
    }

    public virtual Vector2 GetShieldAxis()
    {
        return Vector2.zero;
    }

    public virtual CharacterActions GetCharacterAction()
    {
        return CharacterActions.None;
    }

    public virtual bool GetShieldOpen()
    {
        return false;
    }
}