﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBorder : MonoBehaviour
{
    CameraManager manager;

    public BoxCollider2D boxCollider { get; private set; }

    public int direction { get; set; }

    private void Awake()
    {
        boxCollider = this.GetComponent<BoxCollider2D>();
        manager = this.GetComponentInParent<CameraManager>();
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Player")
        {
            manager.MoveCamera(direction);
        }
    }
}