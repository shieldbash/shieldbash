﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public float bordersOffset;

    public float interpolationSpeed;
    float elapsedInterpolationTime;

    CameraBorder[] borders;

    new Camera camera;

    float sceneAspectRatio;

    Vector3 lastPosition;
    Vector3 targetPosition;

    public float amountMoved { get; private set; }

    private void Awake()
    {
        camera = this.GetComponent<Camera>();

        borders = this.GetComponentsInChildren<CameraBorder>();

        lastPosition = targetPosition = this.transform.position;
    }

    private void Start()
    {
        sceneAspectRatio = (float)Screen.width / (float)Screen.height;

        int direction = 1;
        for (int i = 0; i < borders.Length; i++)
        {
            borders[i].boxCollider.size = new Vector2(1f, this.camera.orthographicSize * 2);

            Vector3 colliderPosition = this.transform.position;
            colliderPosition.x += (this.camera.orthographicSize * sceneAspectRatio + borders[0].boxCollider.size.x / 2f * bordersOffset) * direction;
            colliderPosition.z = 0;

            borders[i].transform.position = colliderPosition;

            borders[i].direction = direction;

            direction *= -1;
        }
    }
    
    void Update()
    {
        if (lastPosition != targetPosition)
        {
            Vector3 newPosition = Vector3.Lerp(lastPosition, targetPosition,elapsedInterpolationTime);

            amountMoved = newPosition.x - this.transform.position.x;

            this.transform.position = newPosition;

            elapsedInterpolationTime += Time.deltaTime / interpolationSpeed;
            

            if (elapsedInterpolationTime >= 1)
            {
                lastPosition = targetPosition;
                this.transform.position = targetPosition;
                elapsedInterpolationTime = 0;
                amountMoved = 0;
            }
        }
    }

    public void MoveCamera(int direction)
    {
        Vector3 newPosition = lastPosition;
        newPosition.x += this.camera.orthographicSize * sceneAspectRatio * 2 * direction;
        
        targetPosition = newPosition;

        if(interpolationSpeed == 0)
        {
            lastPosition = this.transform.position = targetPosition;
        }
    }
}