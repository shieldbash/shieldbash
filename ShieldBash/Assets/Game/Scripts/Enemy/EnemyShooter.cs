﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShotDirections { Up, Down, Left, Right, Player }
public class EnemyShooter : MonoBehaviour
{
    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    ShotDirections shootingDirection;

    [SerializeField]
    float shootingInterval;
    [SerializeField]
    float bulletSpeed;
    [SerializeField]
    float bulletLifetime;

    float shootingCounter;

    private void Update()
    {
        shootingCounter += Time.deltaTime;

        if(shootingCounter>= shootingInterval)
        {
            this.Shoot();

            shootingCounter = 0;
        }
        this.UpdateRotation();
    }

    void UpdateRotation()
    {
        Vector3 direction = Vector3.zero;
        switch (shootingDirection)
        {
            case ShotDirections.Player:
                direction = Character.Instance.transform.position - this.transform.position;
                break;
        }

        this.transform.up = direction;
    }

    void Shoot()
    {
        Vector3 direction = Vector3.zero;
        switch (shootingDirection)
        {
            case ShotDirections.Up:
                direction = Vector3.up;
                break;
            case ShotDirections.Down:
                direction = Vector3.down;
                break;
            case ShotDirections.Left:
                direction = Vector3.left;
                break;
            case ShotDirections.Right:
                direction = Vector3.right;
                break;
            case ShotDirections.Player:
                direction = Character.Instance.transform.position - this.transform.position;
                break;
        }

        Bullet bullet = Instantiate(bulletPrefab,this.transform.position, Quaternion.identity).GetComponent<Bullet>();

        bullet.Setup(direction, bulletSpeed,bulletLifetime);
    }
}