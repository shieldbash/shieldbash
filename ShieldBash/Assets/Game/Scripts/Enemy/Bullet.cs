﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    new Rigidbody2D rigidbody2D;

    float lifetime;
    float lifeCounter;

    public void Setup(Vector3 direction, float speed, float lifetime)
    {
        this.rigidbody2D = this.GetComponent<Rigidbody2D>();

        this.lifetime = lifetime;

        this.transform.up = direction;
        rigidbody2D.velocity = transform.up*speed;
    }

    private void Update()
    {
        lifeCounter += Time.deltaTime;

        if (lifeCounter > lifetime) Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag != "CollidableEnemy" && target.tag != "NonCollidableEnemy")
        {
            Destroy(this.gameObject);
        }
    }
}