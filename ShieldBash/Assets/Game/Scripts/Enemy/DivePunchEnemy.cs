﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DivePunchEnemyStates { Idle, Diving, ReturnUp, ReturnToStart }

public class DivePunchEnemy : MonoBehaviour
{
    DivePunchEnemyStates currentState;

    [Header("Dive Properties")]
    [SerializeField]
    float distanceToDive;

    [SerializeField]
    Transform diveTarget;

    [SerializeField]
    float diveTime;
    [SerializeField]
    AnimationCurve diveCurve;

    float diveCounter;

    [Header("Go Up Properties")]
    [SerializeField]
    float upSpeed;
    [SerializeField]
    float upTime;

    float upCounter;

    [Header("Return To Start Properties")]
    [SerializeField]
    AnimationCurve returnCurve;
    [SerializeField]
    float returnTime;
    float returnCounter;

    Vector3 startPosition;

    Vector3 upPosition;

    private void Awake()
    {
        startPosition = this.transform.position;
    }

    private void Update()
    {
        switch (currentState)
        {
            case DivePunchEnemyStates.Idle:
                this.UpdateIdle();
                break;
            case DivePunchEnemyStates.Diving:
                this.UpdateDiving();
                break;
            case DivePunchEnemyStates.ReturnUp:
                this.UpdateReturnUp();
                break;
            case DivePunchEnemyStates.ReturnToStart:
                this.UpdateReturnToStart();
                break;
        }
    }

    void UpdateIdle()
    {
        if (Vector3.Distance(this.transform.position, Character.Instance.transform.position) < distanceToDive)
        {
            this.currentState = DivePunchEnemyStates.Diving;
        }
    }

    private void UpdateDiving()
    {
        this.transform.position = Vector3.Lerp(startPosition, startPosition + diveTarget.localPosition, diveCurve.Evaluate(diveCounter/diveTime));

        diveCounter += Time.deltaTime;

        if(diveCounter>diveTime)
        {
            this.currentState = DivePunchEnemyStates.ReturnUp;
            diveCounter = 0;
        }
    }

    private void UpdateReturnUp()
    {
        this.transform.position += Vector3.up * Time.deltaTime * upSpeed;

        upCounter += Time.deltaTime;

        if(upCounter > upTime)
        {
            upPosition = this.transform.position;
            this.currentState = DivePunchEnemyStates.ReturnToStart;
            upCounter = 0;
        }
    }

    private void UpdateReturnToStart()
    {
        this.transform.position = Vector3.Lerp(upPosition, startPosition, diveCurve.Evaluate(returnCounter / returnTime));

        returnCounter += Time.deltaTime;

        if(returnCounter > returnTime)
        {
            this.currentState = DivePunchEnemyStates.Idle;
            returnCounter = 0;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, distanceToDive);
    }
}