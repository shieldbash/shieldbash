﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerInputSet : PlayerActionSet
{
    public PlayerAction Left;
    public PlayerAction Right;
    public PlayerAction Up;
    public PlayerAction Down;
    public PlayerTwoAxisAction Move;

    public PlayerAction ShieldLeft;
    public PlayerAction ShieldRight;
    public PlayerAction ShieldUp;
    public PlayerAction ShieldDown;
    public PlayerTwoAxisAction ShieldMove;

    public PlayerAction OpenShield;
    public PlayerAction Jump;
    public PlayerAction DodgeRoll;
    public PlayerAction Run;

    public PlayerInputSet()
    {
        Left = CreatePlayerAction("Move Left");
        Right = CreatePlayerAction("Move Right");
        Up = CreatePlayerAction("Move Up");
        Down = CreatePlayerAction("Move Down");
        Move = CreateTwoAxisPlayerAction(Left, Right, Down, Up);

        ShieldLeft = CreatePlayerAction("Shield Left");
        ShieldRight = CreatePlayerAction("Shield Right");
        ShieldUp = CreatePlayerAction("Shield Up");
        ShieldDown = CreatePlayerAction("Shield Down");
        ShieldMove = CreateTwoAxisPlayerAction(ShieldLeft, ShieldRight, ShieldDown, ShieldUp);

        OpenShield = CreatePlayerAction("Open Shield");
        DodgeRoll = CreatePlayerAction("Dodge Roll");
        Jump = CreatePlayerAction("Jump");
        Run = CreatePlayerAction("Run");
    }

    public static PlayerInputSet CreateWithDefaultBindings()
    {
        PlayerInputSet playerActions = new PlayerInputSet();

        playerActions.Up.AddDefaultBinding(Key.UpArrow);
        playerActions.Down.AddDefaultBinding(Key.DownArrow);
        playerActions.Left.AddDefaultBinding(Key.LeftArrow);
        playerActions.Right.AddDefaultBinding(Key.RightArrow);

        playerActions.Up.AddDefaultBinding(InputControlType.LeftStickUp);
        playerActions.Down.AddDefaultBinding(InputControlType.LeftStickDown);
        playerActions.Left.AddDefaultBinding(InputControlType.LeftStickLeft);
        playerActions.Right.AddDefaultBinding(InputControlType.LeftStickRight);

        //playerActions.Up.AddDefaultBinding(InputControlType.DPadUp);
        //playerActions.Down.AddDefaultBinding(InputControlType.DPadDown);
        //playerActions.Left.AddDefaultBinding(InputControlType.DPadLeft);
        //playerActions.Right.AddDefaultBinding(InputControlType.DPadRight);



        playerActions.ShieldUp.AddDefaultBinding(Mouse.PositiveY);
        playerActions.ShieldDown.AddDefaultBinding(Mouse.NegativeY);
        playerActions.ShieldLeft.AddDefaultBinding(Mouse.NegativeX);
        playerActions.ShieldRight.AddDefaultBinding(Mouse.PositiveX);

        playerActions.ShieldUp.AddDefaultBinding(InputControlType.RightStickUp);
        playerActions.ShieldDown.AddDefaultBinding(InputControlType.RightStickDown);
        playerActions.ShieldLeft.AddDefaultBinding(InputControlType.RightStickLeft);
        playerActions.ShieldRight.AddDefaultBinding(InputControlType.RightStickRight);



        playerActions.OpenShield.AddDefaultBinding(Key.Z);
        playerActions.OpenShield.AddDefaultBinding(InputControlType.Action5);

        playerActions.Jump.AddDefaultBinding(Key.Space);
        playerActions.Jump.AddDefaultBinding(InputControlType.Action3);

        playerActions.DodgeRoll.AddDefaultBinding(Key.Control);
        playerActions.DodgeRoll.AddDefaultBinding(InputControlType.Action4);

        playerActions.Run.AddDefaultBinding(Key.Shift);
        playerActions.Run.AddDefaultBinding(InputControlType.Action2);


        playerActions.ListenOptions.IncludeUnknownControllers = true;
        playerActions.ListenOptions.MaxAllowedBindings = 4;
        playerActions.ListenOptions.UnsetDuplicateBindingsOnSet = true;
        playerActions.ListenOptions.IncludeMouseButtons = true;



        return playerActions;
    }
}