﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerBehaviour : CharacterBehaviour
{
    PlayerInputSet inputSet;

    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        inputSet = PlayerInputSet.CreateWithDefaultBindings();
    }

    public override Vector2 GetMovementAxis()
    {
        return new Vector2(inputSet.Move.X, inputSet.Move.Y);
    }

    public override Vector2 GetShieldAxis()
    {
        return new Vector2(inputSet.ShieldMove.X, inputSet.ShieldMove.Y);
    }

    public override CharacterActions GetCharacterAction()
    {
        CharacterActions currentAction = CharacterActions.None;

        if (inputSet.DodgeRoll.IsPressed && inputSet.DodgeRoll.HasChanged) currentAction = CharacterActions.DodgeRoll;
        if (inputSet.Jump.IsPressed && inputSet.Jump.HasChanged) currentAction = CharacterActions.Jump;
        if (inputSet.Run.IsPressed) currentAction = CharacterActions.Run;

        return currentAction;
    }

    public override bool GetShieldOpen()
    {
        return inputSet.OpenShield.IsPressed;
    }
}