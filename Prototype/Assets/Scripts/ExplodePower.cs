﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodePower : MonoBehaviour
{
    public float energy;
    [SerializeField]
    float duration;
    [SerializeField]
    float maxScale;

    bool activated;

    float life;

    public void Activate()
    {
        activated = true;
        life = 0;
        this.transform.localScale = Vector3.zero;
    }

    public void Deactivate()
    {
        activated = false;
        this.transform.localScale = Vector3.zero;
    }

    void Update()
    {
        if (!activated) return;

        life += Time.deltaTime / duration;

        float scale = this.transform.localScale.x;

        scale = Mathf.Lerp(0, maxScale, life);

        this.transform.localScale = Vector3.one * scale;

        if(life >= 1)
        {
            this.Deactivate();
        }
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        Debug.Log(target.tag);
        if(target.tag == "Shooter" || target.tag == "Bullet")
        {
            Destroy(target.gameObject);
        }
    }
}