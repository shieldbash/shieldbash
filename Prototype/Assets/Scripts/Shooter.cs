﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField]
    Transform player;

    [SerializeField]
    GameObject bullet;

    float counter;

    void Update()
    {
        this.transform.LookAt(player);

        counter -= Time.deltaTime;

        if (counter < 0)
        {
            GameObject.Instantiate(bullet, this.transform.position, this.transform.rotation);

			counter = 5f;//Random.Range(0f, 1f);
        }
    }
}