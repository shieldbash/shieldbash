﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour
{
    [SerializeField]
    Transform shieldSprite;
    
    [SerializeField]
    ExplodePower explodePower;

    SpriteRenderer spriteRenderer;

    Rigidbody2D rigid;
    float gravityScale;
    
    void Start()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();

        shieldSprite.gameObject.SetActive(false);

        rigid = this.GetComponent<Rigidbody2D>();
        gravityScale = rigid.gravityScale;
    }
    
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            if (!shieldSprite.gameObject.activeSelf) shieldSprite.gameObject.SetActive(true);

            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 0;

            shieldSprite.position = this.transform.position;
            shieldSprite.LookAt(mousePosition);
            shieldSprite.position += shieldSprite.forward;

            if(rigid.velocity.y <0)
            {
                rigid.gravityScale = gravityScale / 10f;
            }
            else
            {
                rigid.gravityScale = gravityScale;
            }
        }
        else
        {
            if (shieldSprite.gameObject.activeSelf) shieldSprite.gameObject.SetActive(false);

            rigid.gravityScale = gravityScale;
        }

        if(Input.GetButton("Fire2"))
        {
            this.Explode();
        }

        this.spriteRenderer.color = Color.Lerp(Color.white, Color.red, Shield.instance.currentEnergy / Shield.instance.maxEnergy);
    }

    void Explode()
    {
        Debug.Log("explosion");
        if(Shield.instance.RemoveEnergy(explodePower.energy))
        {
            explodePower.Activate();
        }
    }
}