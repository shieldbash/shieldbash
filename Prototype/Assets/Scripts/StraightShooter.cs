﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightShooter : MonoBehaviour {
	
	[SerializeField]
	GameObject bullet;

	float counter;

	void Update()
	{
		
		counter -= Time.deltaTime;

		if (counter < 0)
		{
			GameObject.Instantiate(bullet, this.transform.position, this.transform.rotation);

			counter = 0.5f;//Random.Range(0f, 1f);
		}
	}
}