﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraReposition : MonoBehaviour {


	[SerializeField]
	Transform gameCamera;

	private Vector3 updatePos; 

	// Use this for initialization
	void Start () {
		updatePos = new Vector3 (this.transform.position.x, 0, gameCamera.transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D ops)
	{
		if (ops.tag == "Player") 
		{
			gameCamera.transform.position = updatePos;
			Debug.Log ("aeeeee" + ops);
		}
	}
}
