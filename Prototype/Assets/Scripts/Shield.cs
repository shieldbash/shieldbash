﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public static Shield instance;

    [SerializeField]
    SpriteRenderer sprite;
    
    public float maxEnergy;
    public float currentEnergy;

    public bool reflecting = false;

    float reflectionTime = 0.2f;
    float reflectionCounter;

    void Awake()
    {
        instance = this;
        currentEnergy = 0;
    }

    void OnEnable()
    {
        this.reflectionCounter = this.reflectionTime;
    }

    void Update()
    {
        reflectionCounter -= Time.deltaTime;

        sprite.color = reflectionCounter > 0 ? Color.green : Color.red;

        reflecting = reflectionCounter > 0;
    }

    public void AddEnergy(float amount)
    {
        currentEnergy = Mathf.Clamp(currentEnergy + amount, 0, maxEnergy);
    }

    public bool RemoveEnergy(float amount)
    {
        if(currentEnergy >= amount)
        {
            currentEnergy -= amount;
            return true;
        }
        return false;
    }
}