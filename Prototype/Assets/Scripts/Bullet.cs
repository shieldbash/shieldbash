﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    float energyToGive = 5;

	[SerializeField]
	float projectileSpeed = 5;

    Vector3 awakeForward;

    float life = 3;

    void Start()
    {
        awakeForward = this.transform.forward;
    }

    void Update()
    {
		this.transform.position += this.transform.forward * Time.deltaTime * projectileSpeed;

        life -= Time.deltaTime;
        if (life < 0) Destroy(this.gameObject);
    }



    void OnTriggerEnter2D(Collider2D target)
    {
		if (target.tag == "Shield") {
			Shield.instance.AddEnergy (energyToGive);

			if (Shield.instance.reflecting) {
				this.transform.forward = awakeForward * -1;
			} else {
				Destroy (this.gameObject);
			}
		} else if (target.tag == "Shooter" && life < 2.6f) {
			Destroy (target.transform.parent.gameObject);
			Destroy (this.gameObject);
		} 
    }
}